---
title: Bienvenue
description: Application de gestion d'entreprises
icon: icon-park-outline:fireworks
---

# Introduction

:ellipsis

Dokos est un logiciel de gestion d'entreprises complet 100% open-source.

::alert{type="info"}
L'application Dokos est une adaptation de l'application ERPNext par <a href="https://github.com/frappe/erpnext" target="_blank">Frappe Technologies</a>
::

::card-grid
#title
Les principaux modules de Dokos

#default
  ::link-card
  ---
  icon: icon-park-outline:buy
  href: /dokos/achats
  ---
  #title
  Achats
  #description
  Découvrir les outils de gestion des achats
  ::

  ::link-card
  ---
  icon: ep:sell
  href: /dokos/ventes
  ---
  #title
  Ventes
  #description
  Découvrir les outils de gestion des ventes
  ::

  ::link-card
  ---
  icon: uil:balance-scale
  href: /dokos/comptabilite
  ---
  #title
  Comptabilité
  #description
  Découvrir les outils de gestion de la comptabilité
  ::

  ::link-card
  ---
  icon: carbon:chart-relationship
  href: /dokos/crm
  ---
  #title
  CRM
  #description
  Découvrir les outils de gestion de la prospection
  ::

  ::link-card
  ---
  icon: material-symbols:location-on-outline-rounded
  href: /dokos/lieu
  ---
  #title
  Lieu
  #description
  Découvrir les outils de gestion d'un lieu
  ::

  ::link-card
  ---
  icon: mdi:human-capacity-increase
  href: /dokos/hrms
  ---
  #title
  Ressources Humaines
  #description
  Découvrir les outils de gestion RH
  ::

  ::link-card
  ---
  icon: fluent:manufacturer-24-regular
  href: /dokos/production
  ---
  #title
  Production
  #description
  Découvrir les outils de gestion de la production
  ::

  ::link-card
  ---
  icon: eos-icons:project-outlined
  href: /dokos/projets
  ---
  #title
  Gestion des projets
  #description
  Découvrir les outils de gestion de projet
  ::

  ::link-card
  ---
  icon: carbon:rule-data-quality
  href: /dokos/qualite
  ---
  #title
  Gestion de la qualité
  #description
  Découvrir les outils de gestion de la qualité
  ::

  ::link-card
  ---
  icon: vaadin:stock
  href: /dokos/stocks
  ---
  #title
  Gestion de stocks
  #description
  Découvrir les outils de gestion des stocks
  ::

  ::link-card
  ---
  icon: ri:customer-service-2-line
  href: /dokos/support
  ---
  #title
  Support
  #description
  Découvrir les outils de gestion de support client
  ::

  ::link-card
  ---
  icon: mdi:connection
  href: /dokos/integrations
  ---
  #title
  Intégrations
  #description
  Découvrir les solutions intégrées avec Dokos
  ::
::