---
title: Introduction
---

# Gestion d'un lieu

:ellipsis

Dokos dispose d'un module de gestion de lieu intégré avec le reste des fonctionnalités du logiciel.  
Il permet de gérer des activités diverses pour répondre aux besoins très variés des différents Tiers Lieux.  

---

## Principales fonctionnalités

::card-grid

#default
  ::link-card
  ---
  href: /dokos/ventes/abonnements
  ---
  #title
  Abonnements
  #description
  Créez des abonnements et collectez les paiements automatiquement
  ::

  ::link-card
  ---
  href: /dokos/lieu/reservations-articles
  ---
  #title
  Réservations
  #description
  Permettez à vos visiteurs de réserver des salles ou des machines
  ::

  ::link-card
  ---
  href: /dokos/lieu/evenements
  ---
  #title
  Événements
  #description
  Gérez vos événements, avec des inscriptions en ligne et une billeterie
  ::

  ::link-card
  ---
  href: /dokos/e-commerce/portal-settings
  ---
  #title
  Site web
  #description
  Créez un site web pour présenter votre lieu et permettre les réservations et inscriptions en ligne
  ::

  ::link-card
  ---
  href: /dokos/lieu/portails
  ---
  #title
  Portail client
  #description
  Permettez à vos visiteurs de s'inscrire en ligne via un formulaire et de gérer leurs abonnements
  ::

  ::link-card
  ---
  href: /dokos/lieu/multi-societe
  ---
  #title
  Multi-lieux
  #description
  Gérez plusieurs lieux dans un seul Doks, et présentez les sur votre site web
  ::
::
