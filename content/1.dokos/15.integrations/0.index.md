---
title: Introduction
---

# Introduction

## 1. Sauvegarde

- [1. Paramètres Dropbox](/dokos/integrations/dropbox-settings)
- [2. Paramètres de sauvegarde S3](/dokos/integrations/s3-backup-settings)
- [3. Google Drive](/dokos/integrations/google-drive)



## 2. Services Google

- [1. Paramètres Google](/dokos/integrations/google-settings)
- [2. Contacts Google](/dokos/integrations/google-contacts)
- [3. Google Agenda](/dokos/integrations/google-calendar)
- [4. Google Drive](/dokos/integrations/google-drive)


## 3. Authentification

::alert{type=warning}
:construction: En cours de rédaction :construction:
::



## 4. Places de marché

- [Paramètres Woocommerce](/integrations/woocommerce)
- [Paramètres Amazon MWS](/dokos/integrations/amazon-mws-settings)
- [Paramètres de Shopify](/dokos/integrations/shopify-settings)


## 5. Paiements

- [GoCardless](/dokos/integrations/gocardless)
- [Stripe](/dokos/integrations/stripe)


## 6. Paramètres

- [Webhook](/dokos/integrations/webhook)
- [URL de webhook entrant](/dokos/integrations/incoming-webhook-url)


## 7. Connecteurs
- [Zapier](/dokos/integrations/zapier)
- [Slack](/dokos/integrations/slack)
- [Google Chat](/dokos/integrations/google-chat)
- [Rocket Chat](/dokos/integrations/rocket-chat)
- [Mattermost](/dokos/integrations/mattermost)