---
title: Règle de livraison
description: 
published: true
date: 2021-08-24T10:21:19.036Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:55:42.182Z
---

# Règle de livraison
En utilisant la règle d'expédition, vous pouvez définir le coût de livraison du produit au client ou depuis le fournisseur.

Vous pouvez définir différentes règles d'expédition ou un montant d'expédition fixe pour le même article dans différents territoires.

---

Pour accéder à la **règle d'expédition**, allez sur :

> Accueil > Vente > Articles et prix > **Règle de livraison**

![règle_de_livraison_.png](/ventes/shipping-rule/règle_de_livraison_.png)

## 1. Comment créer une règle d'expédition

1. Accédez à la liste des **règles d'expédition**, cliquez sur :heavy_plus_sign: Nouveau.
2. Saisissez le libellé de la règle d'expédition, par exemple «Expédition prioritaire» ou «Expédition le jour suivant».
3. Poursuivez avec les détails comptables tels que le compte d'expédition, le centre de coûts auquel le montant sera facturé et le montant d'expédition.
4. Sous Calculer en fonction de, vous pouvez également modifier le calcul sur lequel la règle d'expédition sera appliquée comme la quantité totale nette ou le poids total net, par défaut, il est "Fixe".
5. **Enregistrer**.

![créer_règle_de_livraison.png](/ventes/shipping-rule/créer_règle_de_livraison.png)

## 2. Caractéristiques

### 2.1 Comptabilité
La section comptabilité vous permet de définir la société qui sera affecté par les transactions et de défnir le compte de livraison pour la création de cette règle de livraison. 

![comptabilité_règle_de_livraison.png](/ventes/shipping-rule/comptabilité_règle_de_livraison.png)

### 2.2 Dimensions comptables
Les dimensions comptables vous permettent d'étiqueter les transactions en fonction d'un territoire, d'une succursale, d'un client, etc. spécifiques. Cela permet d'afficher les états comptables séparément en fonction de la ou des dimensions sélectionnées.

### 2.3 Conditions des règles d'expédition

En sélectionnant Total net ou Poids net, un tableau apparaîtra dans lequel vous pouvez définir les valeurs de départ et d'arrivée pour le montant ou le poids. Saisissez le montant d'expédition à calculer pour la plage saisie. Ajoutez plus de conditions si nécessaire. Vous ne pouvez sélectionner qu'une des trois méthodes de calcul dans une règle d'expédition.

![mode_de_calcul_règle_de_livraison.png](/ventes/shipping-rule/mode_de_calcul_règle_de_livraison.png)

### 2.4 Restreindre aux pays

Vous pouvez limiter la règle d'expédition à certains pays, ajoutez les pays dans le tableau. Par défaut, la règle d'expédition sera applicable dans le monde entier.

![restreindre_au_pays_règle_de_livraison.png](/ventes/shipping-rule/restreindre_au_pays_règle_de_livraison.png)





