---
title: Unité de mesure (UDM)
description: 
published: true
date: 2021-05-26T09:58:36.011Z
tags: 
editor: markdown
dateCreated: 2021-05-26T09:57:05.007Z
---

# Unité de mesure (UdM)
Une UdM est une unité à l'aide de laquelle un élément est mesuré.

Par défaut, il existe de nombreuses UdM créées dans Dokos. Cependant, d'autres peuvent être ajoutés en fonction de votre cas d'utilisation professionnel. Dans l'UdM, il existe une option **Doit être un nombre entier**. Si cette case est cochée, vous ne pouvez pas utiliser de nombres de fraction dans cette UoM. Pour en savoir plus sur les fractions et les UdM, consultez cette page.

![liste_udm.png](/content/stocks/uom/liste_udm.png)

La liste UoM seule ne stocke que le nom. Les taux de conversion réels sont stockés dans un document appelé «Facteur de conversion UoM». Si vous ajoutez de nouvelles UoM et prévoyez de les utiliser dans des transactions où elles seront converties en d'autres UoM, il est conseillé de les ajouter à cette liste.

Par exemple, ici 1 kg équivaut à environ 2,2 pound et le facteur de conversion exact est stocké :

![conversion.png](/content/stocks/uom/conversion.png)